import React, { useState, useContext } from "react"
import { Tab, Grid, Header, Button } from "semantic-ui-react"
import ProfileEditForm from "./ProfileEditForm"
import { RootStoreContext } from "../../../app/stores/rootStore"
import { observer } from "mobx-react-lite"

const ProfileDescription = () => {
  const rootStore = useContext(RootStoreContext)
  const { profile, isCurrentUser } = rootStore.profileStore

  const [editMode, setEditMode] = useState(false)

  return (
    <Tab.Pane>
      <Grid>
        <Grid.Column width={16} style={{ paddingBottom: 0 }}>
          <Header
            floated="left"
            icon="user"
            content={`About ${profile!.displayName}`}
          />
          {isCurrentUser && (
            <Button
              onClick={() => setEditMode(!editMode)}
              basic
              floated="right"
              content={editMode ? "Cancel" : "Edit"}
            />
          )}
        </Grid.Column>
        <Grid.Column width={16}>
          {editMode ? (
            <ProfileEditForm setEditMode={setEditMode} />
          ) : (
            <span>{profile!.bio ?? "No bio added"}</span>
          )}
        </Grid.Column>
      </Grid>
    </Tab.Pane>
  )
}

export default observer(ProfileDescription)
