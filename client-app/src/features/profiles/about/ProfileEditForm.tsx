import React, { useContext, useState } from "react"
import { Form, Button } from "semantic-ui-react"
import { Field } from "react-final-form"
import TextInput from "../../../app/common/form/TextInput"
import TextAreaInput from "../../../app/common/form/TextAreaInput"
import { RootStoreContext } from "../../../app/stores/rootStore"
import { Form as FinalForm } from "react-final-form"
import { IProfileFormValues } from "../../../app/models/Profile"
import { observer } from "mobx-react-lite"
import { isRequired } from "revalidate"
import { FORM_ERROR } from "final-form"

interface IProps {
  setEditMode: (editing: boolean) => void
}

const ProfileEditForm: React.FC<IProps> = ({ setEditMode }) => {
  const rootStore = useContext(RootStoreContext)
  const { profile, saveProfile } = rootStore.profileStore

  const [profileFormValues] = useState<IProfileFormValues>(profile!)

  const displayNameValidation = isRequired("Display name")

  const submitProfileFormValues = (values: any) => {
    saveProfile(values)
      .then(() => {
        setEditMode(false)
      })
      .catch((error) => ({
        [FORM_ERROR]: error,
      }))
  }

  return (
    <FinalForm
      onSubmit={submitProfileFormValues}
      initialValues={profileFormValues}
      render={({ handleSubmit, pristine, invalid, submitting }) => (
        <Form onSubmit={handleSubmit} error>
          <Field
            name="displayName"
            value={profile!.displayName}
            component={TextInput}
            validate={displayNameValidation}
          />
          <Field
            name="bio"
            rows={3}
            value={profile!.bio}
            component={TextAreaInput}
          />
          <Button
            loading={submitting}
            floated="right"
            positive
            content="Save profile"
            type="submit"
            disabled={invalid || pristine}
          />
        </Form>
      )}
    />
  )
}

export default observer(ProfileEditForm)
