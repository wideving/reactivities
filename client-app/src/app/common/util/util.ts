import { IActivity, IAttendee } from "../../models/Activity"
import { IUser } from "../../models/User"

export const combineDateAndTime = (date: Date, time: Date) => {
  const dateString = date.toISOString().split("T")[0]
  const timeString = time.toISOString().split("T")[1]
  return new Date(dateString + "T" + timeString)
}

export const setActivityProps = (activity: IActivity, user: IUser) => {
  activity.date = new Date(activity.date)
  activity.isGoing = activity.attendees.some(
    (a) => a.username === user.username
  )
  activity.isHost = activity.attendees.some(
    (s) => s.username === user.username && s.isHost
  )
  return activity
}

export const createAttendee = (user: IUser): IAttendee => {
  return {
    displayName: user.displayName,
    isHost: false,
    username: user.username,
    image: user.image!,
  }
}
