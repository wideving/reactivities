using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Profiles
{
    public class Update
    {
        public class Command : IRequest
        {
            public string DisplayName { get; set; }
            public string Bio { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.DisplayName).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _dataContext;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext dataContext, IUserAccessor userAccessor)
            {
                _userAccessor = userAccessor;
                _dataContext = dataContext;
            }

            public async Task<Unit> Handle(Command request,
                CancellationToken cancellationToken)
            {
                var username = _userAccessor.GetCurrentUsername();

                var profile = await _dataContext.Users
                    .SingleOrDefaultAsync(u => u.UserName == username);

                if (profile.DisplayName == request.DisplayName && profile.Bio == request.Bio)
                {
                    //No changes will be made to database, return success
                    return Unit.Value;
                }

                profile.DisplayName = request.DisplayName;
                profile.Bio = request.Bio;

                var success = await _dataContext.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}